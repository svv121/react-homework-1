import './App.css';
import React from "react";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

class App extends React.Component {
    state = {
        isOpenFirstModal: false,
        isOpenSecondModal: false,
    }

    render() {
        const {isOpenFirstModal, isOpenSecondModal} = this.state;
        return (
            <div className="App">
                <Button
                    backgroundColor = "#FFA8B2"
                    text="Open first modal"
                    onClick={() => {
                        this.setState({isOpenFirstModal: true})
                    }}
                />
                <Button
                    backgroundColor = "#9EFFAC"
                    text="Open second modal"
                    onClick={() => {
                        this.setState({isOpenSecondModal: true})
                    }}
                />
                {
                    isOpenFirstModal &&
                    <Modal
                        header= "Do you want to delete this file?"
                        closeButton = {true}
                        text = {
                        `Once you delete this file, it won't be possible to undo this action.
                         Are you sure you want to delete it?`
                        }
                        RightBtnTxt = "Cancel"
                        bgColor = "#E84C3D"
                        closeModal = {() => {
                            this.setState({isOpenFirstModal: false})
                        }}
                        actions = {<button className="modal__button-left" onClick={() => {
                            alert("You have just deleted this file")
                        }}>Ok</button>}
                    />
                }
                {
                    isOpenSecondModal &&
                    <Modal
                        header = "Take regular breaks. Take one now"
                        closeButton = {false}
                        text = {
                            `It is commonly recommended that you should take a five minute break after every 20 to 30 minutes of continuous activity.`
                        }
                        RightBtnTxt = "Skip"
                        bgColor = "#64B743"
                        closeModal = {() => {
                            this.setState({isOpenSecondModal: false})
                        }}
                        actions = {<button className="modal__button-left" onClick={() => {
                            alert("Good for you")
                        }}>Take a break</button>}
                    />
                }
            </div>
        );
    }
}

export default App;