import React from 'react';
import './Modal.scss';

class Modal extends React.PureComponent {

  render(){
      const { header, closeButton, text, RightBtnTxt, bgColor, closeModal, actions } = this.props;
      return(
          <div className="modal">
              <div className="modal__background"  onClick={closeModal}></div>
              <div className="modal__main-container" style={{backgroundColor: bgColor}}>
                  <header className="modal__header ">{header}</header>
                  {closeButton && <button className="modal__close" onClick={closeModal}>&#9587;</button>}
                  <div className="modal__content-wrapper">{text}</div>
                  <div className="modal__button-wrapper">
                      {actions}
                      <button className="modal__button-right" onClick={closeModal}>{RightBtnTxt}</button>
                  </div>
              </div>
          </div>
      )
 }
}

export default Modal;