import React from 'react';
import styles from './Button.module.scss';

class Button extends React.PureComponent {

	render() {
		const { backgroundColor, onClick, text } = this.props;
		return (
			<button className={styles.buttonsMain} style={{backgroundColor:backgroundColor}} onClick={onClick}>
				{text}
			</button>
		)
	}
}

export default Button;